//
//  FavFoodsApp.swift
//  FavFoods
//
//  Created by Triandy Gunawan Teng on 30/03/23.
//

import SwiftUI

@main
struct FavFoodsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
