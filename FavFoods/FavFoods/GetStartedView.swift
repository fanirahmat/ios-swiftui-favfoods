//
//  GetStartedView.swift
//  FavFoods
//
//  Created by Triandy Gunawan Teng on 24/08/23.
//

import SwiftUI

struct GetStartedView: View {
    @State private var showHome = false
    
    var body: some View {
        VStack(spacing: 0.0) {
            Image("onboarding_illustration")
                .padding(.bottom, 50.0)
            HStack {
                VStack(alignment: .leading, spacing: 0.0) {
                    Text("180K Store")
                        .font(.custom("Poppins-Regular", size: 16))
                        .foregroundColor(Color("colorDarkGray"))
                    Text("Order Your\nFavorite Foods")
                        .font(.custom("Poppins-Bold", size: 36))
                        .foregroundColor(Color("colorNavy"))
                    Button(action: {
                        print("okeeee")
                        showHome = true
                    }, label: {
                        ZStack {
                            RoundedRectangle(cornerRadius: 50)
                                .frame(width: 327, height: 50)
                                .foregroundColor(Color("colorOrange"))
                            Text("Explore Now")
                                .font(.custom("Poppins-SemiBold", size: 16))
                                .foregroundColor(Color("colorWhite"))
                        }
                    })
                    .padding(.top, 30.0)
                }
            }
        }
        .fullScreenCover(isPresented: $showHome, content: {
            HomeView()
        })
    }
}

struct GetStartedView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedView()
    }
}
