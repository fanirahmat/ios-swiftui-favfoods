//
//  DetailsView.swift
//  FavFoods
//
//  Created by Triandy Gunawan Teng on 15/05/24.
//

import SwiftUI

struct DetailsView: View {
    var body: some View {
        
        ScrollView(
            .vertical, showsIndicators: false,
            content: {
                VStack(alignment:.leading, spacing: 20.0) {
                    Image("fruits")
                        .resizable()
                        .cornerRadius(20.0)
                        .frame(width: UIScreen.main.bounds.width - 20, height: 280)
                        
                    HStack(spacing: 16.0) {
                        VStack(alignment: .leading, spacing: 0.0) {
                            Text("Gyoza Sapi")
                                .font(.custom("Poppins-SemiBold", size: 22))
                                .foregroundColor(Color("colorNavy"))
                            
                            Text("Healthy")
                                .font(.custom("Poppins-Regular", size: 16))
                                .foregroundColor(Color("colorGray"))
                            
                        }
                        Spacer()
                        HStack {
                            Text("4.5")
                                .font(.custom("Poppins-SemiBold", size: 18))
                                .foregroundColor(Color("colorNavy"))
                            Image("icon_star")
                        }
                    }
                    
                    
                    Text("Makanan asal Bandung dengan tekstur yang lembut sehingga ketika dimakan rasanya tidak sakit tenggorokan")
                        .font(.custom("Poppins-Regular", size: 18))
                        .foregroundColor(Color("colorNavy"))
                        
                    
                    VStack(alignment: .leading, spacing: 0) {
                        Text("Bundle")
                            .font(.custom("Poppins-SemiBold", size: 18))
                            .foregroundColor(Color("colorNavy"))
                            .padding(.bottom, 12.0)
                        
                        HStack(spacing: 12) {
                            Image("fruits")
                                .resizable()
                                .cornerRadius(12.0)
                                .frame(width: 80, height: 80)
                            Image("fruits")
                                .resizable()
                                .cornerRadius(12.0)
                                .frame(width: 80, height: 80)
                            Image("fruits")
                                .resizable()
                                .cornerRadius(12.0)
                                .frame(width: 80, height: 80)
                        }
                        
                    }.padding(.top, 10.0)
                    
                    HStack(alignment: .top, spacing: 5) {
                        VStack(alignment: .leading, spacing: 0.0) {
                            Text("$808.00")
                                .font(.custom("Poppins-SemiBold", size: 22))
                                .foregroundColor(Color("colorNavy"))
                            Text("/porsi")
                                .font(.custom("Poppins-Regular", size: 16))
                                .foregroundColor(Color("colorGray"))
                        }
                        
                        Spacer()
                        
                        Button(action: {
                            
                        }) {
                            Text("Order Now")
                                .frame(width: 190, height: 50)
                                .font(.custom("Poppins-SemiBold", size: 18))
                                .padding(.vertical, 5)
                                .padding(.horizontal, 10)
                                .foregroundColor(.white)
                                .background(Color("colorOrange"))
                                .cornerRadius(50)
                        }
                    }
                    
                    
                    
                    
                    
                }
                .padding(.horizontal, 20.0)
            })
        
    }
}

#Preview {
    DetailsView()
}
