//
//  HomeView.swift
//  FavFoods
//
//  Created by Triandy Gunawan Teng on 24/08/23.
//

import SwiftUI

struct HomeView: View {
    
    @State private var searchFood: String = ""
    @State private var showDetailView = false
    
    var body: some View {
        NavigationStack {
            ScrollView(.vertical, showsIndicators: false, content: {
                VStack(alignment: .leading, spacing: 0.0) {
                    HStack {
                        Image("user_photo")
                        Spacer()
                        VStack(alignment: .trailing) {
                            Text("Howdy")
                                .font(.custom("Poppins-Regular", size: 14))
                                .foregroundColor(Color("colorDarkGray"))
                            Text("Lunar Polar")
                                .font(.custom("Poppins-SemiBold", size: 16))
                                .foregroundColor(Color("colorNavy"))
                        }
                    }
                    
                    
                    VStack(alignment: .center) {
                        Text("What do you want\nfor lunch?")
                            .font(.custom("Poppins-Bold", size: 22))
                            .foregroundColor(Color("colorNavy"))
                            .multilineTextAlignment(.center)
                        
                    }
                    .frame(minWidth: 0, maxWidth: .infinity,  minHeight: 66, maxHeight: 66)
                    .padding(.top, 30.0)
                    .padding(.bottom, 24.0)
                    
                    
                    HStack {
                        TextField("Search pizza, burger, etc ...", text: $searchFood)
                            .padding(.leading, 24.0)
                            .disableAutocorrection(true)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 50, maxHeight: 50)
                    .background(Color("colorSoftGray"))
                    .cornerRadius(/*@START_MENU_TOKEN@*/50.0/*@END_MENU_TOKEN@*/)
                    
                    
                    VStack(alignment: .leading, spacing: 0) {
                        Text("Categories")
                            .font(.custom("Poppins-SemiBold", size: 16))
                            .foregroundColor(Color("colorNavy"))
                            .padding(.bottom, 12.0)
                        
                        ScrollView(.horizontal, showsIndicators: false, content: {
                            HStack {
                                HStack {
                                    Image("cate_bread")
                                        .padding(.vertical, 5.0)
                                        .padding(.leading, 5.0)
                                    Text("Bread")
                                        .font(.custom("Poppins-Medium", size: 16))
                                        .foregroundColor(Color("colorNavy"))
                                        .padding(.trailing, 18.0)
                                }
                                .background(Color("colorSoftGray"))
                                .cornerRadius(/*@START_MENU_TOKEN@*/50.0/*@END_MENU_TOKEN@*/)
                                
                                HStack {
                                    Image("cate_carrot")
                                        .padding(.vertical, 5.0)
                                        .padding(.leading, 5.0)
                                    Text("Healthy")
                                        .font(.custom("Poppins-Medium", size: 16))
                                        .foregroundColor(Color("colorNavy"))
                                        .padding(.trailing, 18.0)
                                }
                                .background(Color("colorSoftGray"))
                                .cornerRadius(50.0)
                                
                                HStack {
                                    Image("cate_donut")
                                        .padding(.vertical, 5.0)
                                        .padding(.leading, 5.0)
                                    Text("Sweet")
                                        .font(.custom("Poppins-Medium", size: 16))
                                        .foregroundColor(Color("colorNavy"))
                                        .padding(.trailing, 18.0)
                                }
                                .background(Color("colorSoftGray"))
                                .cornerRadius(/*@START_MENU_TOKEN@*/50.0/*@END_MENU_TOKEN@*/)
                            }
                            
                        })
                        
                    }
                    .padding(.top, 30.0)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        Text("Most Ordered")
                            .font(.custom("Poppins-SemiBold", size: 16))
                            .foregroundColor(Color("colorNavy"))
                            .padding(.bottom, 12.0)
                        
                        
                        NavigationLink {
                            DetailsView()
                        } label: {
                            HStack(spacing: 16) {
                                Image("food_orange")
                                VStack(alignment: .leading, spacing: 0) {
                                    Text("Orange Asem")
                                        .font(.custom("Poppins-Medium", size: 16))
                                        .foregroundColor(Color("colorNavy"))
                                    
                                    Text("Healthy")
                                        .font(.custom("Poppins-Regular", size: 15))
                                        .foregroundColor(Color("colorGray"))
                                    
                                }
                                Spacer()
                                HStack {
                                    Text("4.5")
                                        .font(.custom("Poppins-SemiBold", size: 16))
                                        .foregroundColor(Color("colorNavy"))
                                    Image("icon_star")
                                }
                            }
                        }
                        
                        
                        HStack(spacing: 16) {
                            Image("food_gyoza")
                            VStack(alignment: .leading, spacing: 0) {
                                Text("Gyoza Sapi")
                                    .font(.custom("Poppins-Medium", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                
                                Text("Meal")
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .foregroundColor(Color("colorGray"))
                                
                            }
                            Spacer()
                            HStack {
                                Text("4.3")
                                    .font(.custom("Poppins-SemiBold", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                Image("icon_star")
                            }
                        }
                        
                        HStack(spacing: 16) {
                            Image("food_avocado")
                            VStack(alignment: .leading, spacing: 0) {
                                Text("Avocado Salad")
                                    .font(.custom("Poppins-Medium", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                
                                Text("Healthy")
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .foregroundColor(Color("colorGray"))
                                
                            }
                            Spacer()
                            HStack {
                                Text("4.0")
                                    .font(.custom("Poppins-SemiBold", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                Image("icon_star")
                            }
                        }
                        
                        HStack(spacing: 16) {
                            Image("food_avocado")
                            VStack(alignment: .leading, spacing: 0) {
                                Text("Avocado Salad")
                                    .font(.custom("Poppins-Medium", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                
                                Text("Healthy")
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .foregroundColor(Color("colorGray"))
                                
                            }
                            Spacer()
                            HStack {
                                Text("4.2")
                                    .font(.custom("Poppins-SemiBold", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                Image("icon_star")
                            }
                        }
                        
                        HStack(spacing: 16) {
                            Image("food_avocado")
                            VStack(alignment: .leading, spacing: 0) {
                                Text("Avocado Salad")
                                    .font(.custom("Poppins-Medium", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                
                                Text("Healthy")
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .foregroundColor(Color("colorGray"))
                                
                            }
                            Spacer()
                            HStack {
                                Text("4.2")
                                    .font(.custom("Poppins-SemiBold", size: 16))
                                    .foregroundColor(Color("colorNavy"))
                                Image("icon_star")
                            }
                        }
                        
                    }
                    .padding(.top, 30.0)
                    
                }
                .padding(.horizontal, 24.0)
                
            })
        }
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
