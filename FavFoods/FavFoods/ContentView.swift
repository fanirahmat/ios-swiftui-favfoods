//
//  ContentView.swift
//  FavFoods
//
//  Created by Triandy Gunawan Teng on 30/03/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        GetStartedView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
